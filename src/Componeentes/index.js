import React, { useEffect, useState } from 'react';
import { View, Text, TextInput, Button, FlatList, StyleSheet, SafeAreaView } from 'react-native';
import usuarioService from '../Services/api';
import Ionicons from '@expo/vector-icons/Ionicons';

export default function Componeentes() {
    return (
        <View>
            <Text>
                TITULO TELA
            </Text>
        </View>
    );
}

export function Titulo() {
    return (
        <View>
            <Text>
                A tela do tipo FORMULARIO exibe uma coleção de campos (itens) e possui um ou dois botões de ação na parte inferior. A seguir temos um exemplo do objeto JSON e de como a tela é exibida pelo app.
            </Text>
        </View>
    );
}

export function Input({ placeholder, text }) {
    return (
        <View>
            <Text style={{ marginTop: 20 }} >
                {text}
            </Text>
            <TextInput
                style={{ paddingVertical: 0, outline: 'none', borderBottomWidth: 1, width: 400, marginTop: 10 }}
                placeholder={placeholder}
                placeholderTextColor={"black"}>
            </TextInput>
        </View>
    );
}

export function Buton({ text, color, colorButton }) {
    return (
        <View style={{
            marginRight: 40,
            marginLeft: 40,
            paddingTop: 8,
            backgroundColor: color,
            borderRadius: 10,
            borderWidth: 1,
            borderColor: '#53ac59',
            width: 140,
            height: 55
        }}>
            <Button
                color={colorButton}
                title={text}>
            </Button>
        </View>
    );
}

export function Espacamento({ space }) {
    return (
        <View style={{ paddingTop: space }}>
        </View>
    );
}

export function Pautas() {

    const [dados, setDados] = useState("")

    useEffect(() => {
        usuarioService.dados()
            .then((response) => {
                setDados(response.data)
                console.log(response.data)
                return Promise.resolve(response)
            }).catch((error) => {
                return Promise.reject(error)
            })
    }, []);

    const Item = ({ title }) => (
        <View style={styles.item}>
            <Text style={styles.title}>{title}</Text>
            
            <View style={{marginLeft: 200}} >
            <Icone nameIcone={"chevron-forward"} tamanhoIcon={22} corIcone={"#53ac59"} />
            </View>
     
        </View>
    );

    return (
        <SafeAreaView style={{ alignSelf: 'flex-start', marginLeft: 15, marginTop: 100 }}>
            <FlatList
                data={dados}
                renderItem={({ item }) => <Item title={item.titulo} />}
                keyExtractor={item => item.id}
            />
        </SafeAreaView>
    );
};

export function Icone({ nameIcone, tamanhoIcon, corIcone, ...rest }) {
    return (
        <Ionicons name={nameIcone} size={tamanhoIcon} color={corIcone} {...rest} />
    );
}

const styles = StyleSheet.create({
    item: {
        flexDirection: 'row',
        marginTop: 20,
        paddingTop: 5


    },
    title: {

        fontSize: 20,
    },
});
