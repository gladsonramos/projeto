import axios from "axios"

const link = 'http://desafioact-env.eba-mnwd67ie.us-east-1.elasticbeanstalk.com/pautas'

class UsuarioService{   


    async dados(){
  
        return axios({
            url: link,
            method: "GET",
            timeout: 5000,
        }).then((response) => {
            
            return Promise.resolve(response)
           
        }).catch((error) => {
         
            return Promise.reject(error)
        })

       
    }

   
}

const usuarioService = new UsuarioService()
export default usuarioService