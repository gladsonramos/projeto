import React from 'react';
import { View, Text } from 'react-native';
import Componeentes, { Buton, Espacamento, Input, Titulo } from '../Componeentes';

export default function Cadastro() {
    return (
        <View style={{ marginTop: 50, alignItems: 'center' }} >
            <Componeentes />
            <Espacamento space={20} />
            <Titulo />
            <Espacamento space={20} />
            <Input placeholder={"texto"} text={"campo de texto"} />
            <Input placeholder={"999"} text={"campo numerico"} />
            <Input placeholder={"2092-02-02"} text={"campo de data"} />
            <Espacamento space={400} />
            <View style={{ flexDirection: 'row' }}>
                <Buton text={"Cancelar"} color={"white"} colorButton={"#53ac59"} />
                <Buton text={"Ação 1"} color={"#53ac59"} colorButton={"white"} />
            </View>
        </View>
    );
}